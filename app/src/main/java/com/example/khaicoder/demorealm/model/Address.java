package com.example.khaicoder.demorealm.model;

import io.realm.RealmObject;

public class Address extends RealmObject {
    private String city;
    private String nationality;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return
               city + " " +
                " " + nationality
                ;
    }
}
