package com.example.khaicoder.demorealm;

import com.example.khaicoder.demorealm.model.Person;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObject;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class MyMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();


        if (oldVersion == 0) {
            schema.get("Person")
                    .addField("school", String.class);
            oldVersion++;
        }
        if (oldVersion == 1) {
            schema.get("Person")
                    .addRealmListField("b", schema.get("Address"));
            oldVersion++;
        }
        if (oldVersion == 2) {
            schema.get("Person")
                    .addRealmListField("adr", schema.get("Address"));
            oldVersion++;
        }
        if (oldVersion == 3) {
            schema.get("Person")
                    .addRealmObjectField("addres", schema.get("Address"));
            oldVersion++;
        }

        if (oldVersion==4){
            RealmObjectSchema sch = schema.create("Demo")
                    .addField("str",String.class);

            schema.get("Person")
                    .addRealmObjectField("demos",sch);
            oldVersion++;
        }

    }
}
