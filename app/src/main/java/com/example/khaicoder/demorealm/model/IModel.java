package com.example.khaicoder.demorealm.model;

import io.realm.RealmResults;

public interface IModel {
    RealmResults<Person> findAllModel();
    RealmResults<Person> findNameModel(String name);
}
