package com.example.khaicoder.demorealm.pre;

import com.example.khaicoder.demorealm.model.Person;

import io.realm.RealmResults;

public interface IManager {
    void insert(Person person);

    void update(Person person);

    void deleteAll();

    void deleteName(String name);

    RealmResults<Person> findAll();

    RealmResults<Person> findName(String name);

    void closeRealm();
}
