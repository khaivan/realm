package com.example.khaicoder.demorealm.pre;

import android.os.AsyncTask;
import android.util.Log;

import com.example.khaicoder.demorealm.view.IView;
import com.example.khaicoder.demorealm.model.ManagerModel;
import com.example.khaicoder.demorealm.model.Person;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class Manager implements IManager {
    private Realm realm;
    private RealmResults<Person> persons;
    private IView iView;
    private ManagerModel managerModel;
    private RealmAsyncTask realmAsyncTask;

    public Manager(IView iView) {
        this.iView = iView;
        managerModel = new ManagerModel(this);
    }

    @Override
    public void insert(final Person person) {

        realm = Realm.getDefaultInstance();
        realmAsyncTask = realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Person person1 = realm.where(Person.class).equalTo("name", person.getName()).findFirst();
                person1.setAge(person.getAge());

            }

        });


    }

    //insert or update
    @Override
    public void update(final Person person) {
        realm = Realm.getDefaultInstance();
        realmAsyncTask =  realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(person);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                iView.succes();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                iView.error();
            }
        });

    }

    @Override
    public void deleteAll() {
        realm = Realm.getDefaultInstance();
        realmAsyncTask =  realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                iView.succes();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                iView.error();
            }
        });
    }


    @Override
    public void deleteName(final String name) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                delete(name);
                return null;
            }
        }.execute();

    }

    private void delete(final String name) {
        realm = Realm.getDefaultInstance();
        realmAsyncTask = realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                persons = realm.where(Person.class).equalTo("name",
                        name).findAll();
                if (persons.size() == 0) {
                    return;
                }
                persons.deleteAllFromRealm();
            }
        });
    }

    @Override
    public RealmResults<Person> findAll() {
        return managerModel.findAllModel();
    }

    @Override
    public RealmResults<Person> findName(String name) {
        return managerModel.findNameModel(name);
    }

    @Override
    public void closeRealm() {
        realm.close();
        realmAsyncTask.cancel();
    }


}
