package com.example.khaicoder.demorealm.model;

import com.example.khaicoder.demorealm.pre.IManager;

import io.realm.Realm;
import io.realm.RealmResults;

public class ManagerModel implements IModel {
    private IManager iManager;
    private RealmResults<Person> persons;
    private Realm realm = Realm.getDefaultInstance();
    public ManagerModel(IManager iManager) {
        this.iManager = iManager;
    }


    @Override
    public RealmResults<Person> findAllModel() {
        realm.beginTransaction();
        persons = realm.where(Person.class).findAllAsync();
        realm.commitTransaction();
        return persons;
    }

    @Override
    public RealmResults<Person> findNameModel(String name) {
        realm.beginTransaction();
        persons = realm.where(Person.class)
                .equalTo("name", name)
                .findAllAsync();
        realm.commitTransaction();
        return persons;
    }
}
