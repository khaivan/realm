package com.example.khaicoder.demorealm.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khaicoder.demorealm.R;
import com.example.khaicoder.demorealm.model.Address;
import com.example.khaicoder.demorealm.model.Person;
import com.example.khaicoder.demorealm.pre.Manager;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements IView {
    private EditText edtName, edtAge, edtCity, edtNation;
    private Manager manager;
    private TextView tvContent;
    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = new Manager(this);
        initsView();
        initsAction();

    }

    private void initsAction() {
        findViewById(R.id.btn_insert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPerson();
                manager.insert(person);

            }
        });

        findViewById(R.id.btn_find_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = "";
                RealmResults<Person> p = manager.findAll();
                for (Person per : p
                        ) {
                    str += per.toString();
                }
                tvContent.setText(str);
            }
        });

        findViewById(R.id.btn_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPerson();
                manager.update(person);

            }
        });

        findViewById(R.id.btn_delete_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.deleteAll();
            }
        });
        findViewById(R.id.btn_delete_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtName.getText().toString();
                manager.deleteName(name);
            }
        });

        findViewById(R.id.btn_find_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtName.getText().toString();
                show(manager.findName(name));
            }
        });
    }

    private void show(RealmResults<Person> p) {
        String str = "";
        for (Person per : p
                ) {
            str += per.toString();
        }
        tvContent.setText(str);
    }

    private void initPerson() {
        person = new Person();
        Address address = new Address();
        address.setCity(edtCity.getText().toString());
        address.setNationality(edtNation.getText().toString());
        person.setName(edtName.getText().toString());
        person.setAge(Integer.parseInt(edtAge.getText().toString()));
        person.setAddress(address);
    }

    private void initsView() {
        edtName = findViewById(R.id.edt_name);
        edtAge = findViewById(R.id.edt_age);
        edtCity = findViewById(R.id.edt_city);
        edtNation = findViewById(R.id.edt_nation);
        tvContent = findViewById(R.id.tv_contents);

    }

    @Override
    public void succes() {
        Toast.makeText(this, "succes", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void error() {
        Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.closeRealm();
    }
}
