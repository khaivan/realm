package com.example.khaicoder.demorealm;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("demorealm")
                .schemaVersion(5)
                .migration(new MyMigration())
                .build();


        Realm.setDefaultConfiguration(configuration);
    }
}
