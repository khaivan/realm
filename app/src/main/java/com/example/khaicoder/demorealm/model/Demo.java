package com.example.khaicoder.demorealm.model;

import io.realm.RealmObject;

public class Demo extends RealmObject {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
