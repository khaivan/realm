package com.example.khaicoder.demorealm.view;

import com.example.khaicoder.demorealm.model.Person;

import io.realm.RealmResults;

public interface IView {
    void succes();
    void error();
}
